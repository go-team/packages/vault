## https://www.vaultproject.io/docs/configuration/storage/filesystem.html
storage "file" {
    path = "/var/lib/vault"
}
