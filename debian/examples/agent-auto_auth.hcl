## https://www.vaultproject.io/docs/agent/autoauth/index.html
auto_auth {

#    ## https://www.vaultproject.io/docs/agent/autoauth/methods/index.html
#    method "cert" {
#        role = "me"
#    }

#    ## https://www.vaultproject.io/docs/agent/autoauth/methods/aws.html
#    method "aws" {
#        mount_path = "auth/aws-subaccount"
#        config = {
#            type = "iam"
#            role = "foobar"
#        }
#    }

    ## https://www.vaultproject.io/docs/agent/autoauth/sinks/file.html
    sink "file" {
        config = {
            path = "/tmp/file-foo"
        }
    }
}
