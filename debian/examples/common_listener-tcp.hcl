## https://www.vaultproject.io/docs/configuration/listener/tcp.html
listener "tcp" {
    address         = "127.0.0.1:8200"
    tls_disable     = 1
    # tls_cert_file = "/etc/ssl/vault.crt"
    # tls_key_file  = "/etc/ssl/vault.key"
}
